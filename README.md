# flutter-webrtc-server
A simple WebRTC Signaling server html5.

## Compatible browser
Chrome/Firefox/Safari

## Usage
- Clone the repository, run `npm install`.  
- Run `npm start` and open https://your_sever_ip_or_domain:8086 to use html5 demo. 
