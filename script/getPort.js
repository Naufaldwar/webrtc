const fs = require('fs');

fs.writeFileSync('./config/port.js', 'export const SERVER_PORT = ' + process.env.PORT + ';\n');
console.log(`Your server port is ${process.env.PORT}`);

fs.appendFileSync('./config/port.js', 'export const CLIENT_PORT = ' + process.env.CLIENT_PORT + ';\n');
console.log(`Your client port is ${process.env.CLIENT_PORT}`);
